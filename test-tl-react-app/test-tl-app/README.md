


#Prerequisite

1). NodeJS
2). Yarn

#Initiate react app
npm install -g create-react-app

#Create React Application with typescript dependencies
create-react-app test-tl-app  --scripts-version=react-scripts-ts


#Generate Client code using Swagger Codegen
#Clone Swagger-codegen api to local system
git clone https://github.com/swagger-api/swagger-codegen

cd swagger-codegen

#Generate Client code
java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate -i D:/KTworkspace/test-tl-react-app/test-tl-app/tourleader-api.yaml  -l typescript-fetch -o D:/KTworkspace/test-tl-react-app/test-tl-app/src/api

# Install portable-fetch if require
npm install --save portable-fetch es6-promise


Change const BASE_PATH = "http://localhost:8080/v1" under "src/api/api.ts" to URL where Api's are published 





