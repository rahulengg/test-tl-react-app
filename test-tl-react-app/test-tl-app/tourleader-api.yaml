swagger: '2.0'
info:
  title: Kuoni Tumlare Tourleader API
  description: Tourleader 
  version: "1.0.0"

host: api.tourlead.com

schemes:
  - https

basePath: /v1
produces:
  - application/json
paths:
  /tourleader/{tourleader_id}/tours:
    get:
      summary: Itinerary List
      description: |
        The Tours endpoint returns list of tourleader's tour numbers
      parameters:
        - name: tourleader_id
          in: path
          description: Tourleader identification
          required: true
          type: string
      tags:
        - Tourleader
      responses:
        200:
          description: Tour List
          schema:
            type: array
            items:
              type: string
            example: ["7897897897", "KN5655445", "ASD2565444"]
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
        
            
  /tour/{tour_number}:
    get:
      summary: Tour Information
      description: |
        The Itinerary endpoint returns detail information about tour e.g. final backend for tour number 
      parameters:
        - name: tour_number
          in: path
          description: Tour number
          required: true
          type: string
      tags:
        - Tourleader
      responses:
        200:
          description: Tour Object
          schema:
            $ref: '#/definitions/Tour'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
  
  /itinerary/{backend}/{tour_number}:
    get:
      summary: Tour Information
      description: |
        The Itinerary endpoint returns detail information about tour e.g. final backend for tour number 
      parameters:
        - name: backend
          in: path
          description: Backend System Name
          required: true
          type: string
          enum: [kuoni, tumlare]
        - name: tour_number
          in: path
          description: Tour number
          required: true
          type: string
      tags:
        - Tourleader
      responses:
        200:
          description: Itinerary Object
          schema:
            $ref: '#/definitions/Itinerary'
          examples:
            application/json:
              {
                'additional_pax_count': 0,
                'agent_name': 'John Doe',
                'pax_count': 20,
                'tour_number': 'CLLO001474',
                'list': [
                  {
                    'city': 'Praha',
                    'country': 'CZ',
                    'end': '2018-01-01T10:00:00.000+0000',
                    'id': '645189',
                    'start': '2017-12-30T15:00:00.000+0000'
                  },
                  {
                    'city': 'Warsaw',
                    'country': 'PL',
                    'end': '2018-01-04T10:00:00.000+0000',
                    'id': '645190',
                    'start': '2018-01-02T15:00:00.000+0000'
                  },
                  {
                    'city': 'Berlin',
                    'country': 'DE',
                    'end': '2018-01-06T10:00:00.000+0000',
                    'id': '645189',
                    'start': '2018-01-05T15:00:00.000+0000'
                  },
                  {
                    'city': 'Hamburg',
                    'country': 'DE',
                    'end': '2018-01-10T10:00:00.000+0000',
                    'id': '645189',
                    'start': '2018-01-07T15:00:00.000+0000'
                  },
                  {
                    'city': 'London',
                    'country': 'UK',
                    'end': '2018-01-13T10:00:00.000+0000',
                    'id': '645189',
                    'start': '2018-01-11T15:00:00.000+0000'
                  },
                  {
                    'city': 'Reykjavík',
                    'country': 'IS',
                    'end': '2018-01-20T10:00:00.000+0000',
                    'id': '645189',
                    'start': '2018-01-14T15:00:00.000+0000'
                  },
                  {
                    'city': 'Trondheim',
                    'country': 'NO',
                    'end': '2018-01-22T10:00:00.000+0000',
                    'id': '645189',
                    'start': '2018-01-21T15:00:00.000+0000'
                  }
                ]
              }
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
            
  
definitions:

  Itinerary:
    type: object
    properties:
      tour_number:
        type: string
        description: Unique identifier representing a specific tour.
        example: CLLO001474
      agent_name:
        type: string
        description: Display name of product.
        example: John Doe
      pax_count:
        type: integer
        format: int32
        description: Count of passengers.
        example: 20
      additional_pax_count:
        type: integer
        format: int32
        description: Count of additional passengers.
        example: 0
      list:
        type: array
        items:
          $ref: '#/definitions/ItineraryItem'
          
  ItineraryItem:
    type: object
    properties:
      id:
        type: string
        description: Unique identifier representing a specific Itinerary Item
        example: 645189
      start:
        type: string
        format: date-time
        description: "Start date time of itinerary detail - format: https://tools.ietf.org/html/rfc3339#section-5.6"
        example: 2017-12-30T15:00:00Z
      end:
        type: string
        format: date-time
        description: "End date time of itinerary detail - format: https://tools.ietf.org/html/rfc3339#section-5.6"
        example: 2018-01-01T10:00:00Z
      country:
        type: string
        pattern: '^[A-Z]{2}$'
        description: "Country in iso 3166-1-alpha-2 format - more information can be taken from API https://restcountries.eu"
        example: CZ
      city:
        type: string
        pattern: '^[A-Z]{3}$'
        description: "City name"
        example: Praha
  Tour:
    type: object
    properties:
      id:
        type: string
        description: Unique identifier representing a specific Tour
        example: 645189
      backend:  
        type: string
        enum: [kuoni, tumlare]
        example: tumlare
        
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
      fields:
        type: string
