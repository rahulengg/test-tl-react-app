import * as React from 'react';
import {Col,Grid,Row,} from 'react-bootstrap';
import {Jumbotron} from 'react-bootstrap';
import { Link } from 'react-router';
import {Tour,TourleaderApi} from './api/api';


const tourDetail=new TourleaderApi();

interface IState {
    tourDtl: Tour;
}

interface Iprops {

  params : {tourNo : string , companyName:"kuoni" | "Tumlare"}
}

export class TourInfo extends React.Component<Iprops,IState > {

    constructor(prop:Iprops){
        super(prop);
        this.state={ 
            tourDtl :{
                id: '',
            }
        };
       
    }

    public componentDidMount(){
        tourDetail.tourTourNumberGet(this.props.params.tourNo).then((tourDtl)=>{
               this.setState({tourDtl});
            });
     }

    public render() {
        return (
            <div >
           <Jumbotron>

             <p>(A) E672TS EK ITALY 8 )<br />HANKYU TRAVEL INTERNATIONAL<br />
             <p>	18-09-2017-25-09-2017<br/>33+0(Pax+TC)</p>	
             
                {this.props.params.tourNo}</p>  
               
                <p><Link to="/itinerary">{this.state.tourDtl.id}</Link><br/>
                {this.state.tourDtl.backend}</p>

          </Jumbotron>

           <Grid className="App-title">
            <Row>
              <Col xs={12} md={12} >
                <div className="well">						
					        <p className="App-title"><Link to="/itinerary">Tour Itinerary</Link></p><br/>						
			          </div>		
                  <hr className="half-rule"/>
                <div className="well">						
						      <p className="App-title"><Link to="/ItineraryDtl">Hotel List</Link></p><br/>					
		          	</div>	
            	     <hr className="half-rule"/>
                 <div className="well">						
						        <p className="App-title"><Link to="/ItineraryDtl">Meal Plan</Link></p><br/>	
			           </div>	
             	      <hr className="half-rule"/>
                 <div className="well">						
						        <p className="App-title"><Link to="/ItineraryDtl">Important Numbers</Link></p><br/>	
			           </div>	
                    <hr className="half-rule"/>               
                 <div className="well">						
						         <p className="App-title"><Link to="/ItineraryDtl">Weather Information</Link></p><br/>	
			           </div>	
             	       <hr className="half-rule"/>
                 <div className="well">						
					            <p className="App-title"><Link to="/ItineraryDtl">Messages</Link></p><br/>	
			           </div>	
                     <hr className="half-rule"/>                                     
                </Col>
              </Row>
            </Grid>
         </div>
            
        );
    }
}