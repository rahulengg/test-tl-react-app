import * as React from 'react';
import {Button,Col,Grid,Row, Tooltip} from 'react-bootstrap';
import {Link} from 'react-router';
import {TourleaderApi} from './api/api';
import './App.css';

const data=new TourleaderApi();

interface IState {
    tourListData:any[],
 }

class TourScreen extends React.Component<{},IState> {
   constructor(){
        super(React);
        this.state = {
            tourListData: [],
        };
    }
  public componentDidMount()
     {
        data.tourleaderTourleaderIdToursGet('TEST0123').then((tourListData)=>{this.setState({tourListData});});
     }

  public render() {
    return (
            <div className="container">
                <Tooltip placement="left" className="in" >
                   Welcome User!!<br />
                      Get your Tour Info...
                </Tooltip>
                {this.props.children}
                  <div className="Tour" >
                     { this.state.tourListData.map((member) =>
                       this.printTourNo(member))
                     }
               </div>
          </div>        
    );
  }
  private printTourNo(tourNo: string): JSX.Element {    
    const linkUrl="/TourInfo/";
    return  (         
     <div>
           <hr className="half-rule"/>
           <li className="App-title"><Link to={linkUrl+tourNo}><button>{tourNo}</button></Link> </li>
            <Grid>
             <Row>
                  <Col xs={6} md={4} >
                    <div className="tour_Details">						
  				             <p>Test Tour<br />Test Agent</p>	<br/>							
  				                <p>18-09-2017-25-09-2017<br/>33+0(Pax+TC)</p>								
  		               </div>		
                  </Col>
                <Col xs={6} md={2}>           
                  <Link to={linkUrl+tourNo}><Button>View Tour Info</Button></Link>
                </Col>
              </Row>
            </Grid>
    </div>  
   );
  }
}


export default TourScreen;

