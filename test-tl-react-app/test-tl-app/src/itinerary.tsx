
import * as React from 'react';
import { Badge } from 'react-bootstrap';
import {Itinerary, TourleaderApi} from './api/api';

const tourDetail=new TourleaderApi();

interface IState {
    itineraryDtl: Itinerary;
}

export class ItineraryData extends React.Component<{},IState > {

    constructor(){
        super(React);
        this.state={
            itineraryDtl:{
                additionalPaxCount: -1,
                agentName: '',
                list: [
                    {
                        city: '',
                        country: '',
                        end: new Date,
                        id: '',
                        start: new Date,
                    }
                ],
                paxCount: -1,
                tourNumber: '',
            }
        };
       
    }

  public itenararyList() 
  {
    return  this.state.itineraryDtl.list!.map((list1) =>
      <div key = {list1.id} >
            <hr className="half-rule"/>
            <p><b>Id : </b>{list1.id}<br />
            <b>City Name : </b>{list1.city}<br />
            <b>Country Name : </b>{list1.country}<br />
            <b>Start : </b>{list1.start!.toString()}<br />
            <b>End : </b>{list1.end!.toString()}</p>      
      </div>
    );
  }


  public tourList(itinerary: Itinerary) 
  {
    return(
      <div>
          <p>Tour No : {itinerary.tourNumber}</p>
          <p>Additional Pax count : {itinerary.additionalPaxCount}</p>
          <p>AgentName : {itinerary.agentName}</p>
          <p>Pax count : {itinerary.paxCount}</p>     
      </div>
    );
  }

    public render() {
        return (
            <div className="container">
               <br/>
               <Badge>
                      {this.tourList(this.state.itineraryDtl)}
               </Badge>
               { this.itenararyList() }
             </div>
        );
    }

  public componentDidMount()
     {
        tourDetail.itineraryBackendTourNumberGet('tumlare','TEST0123').then((itineraryDtl)=>{
               this.setState({itineraryDtl});
            });
     }
}