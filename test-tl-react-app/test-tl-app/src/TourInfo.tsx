import * as React from 'react';
import { Badge } from 'react-bootstrap';
import {Col,Grid,Row,} from 'react-bootstrap';
import { Link } from 'react-router';
import {Tour,TourleaderApi} from './api/api';


const tourDetail=new TourleaderApi();

interface IState {
    tourDtl: Tour;
}

interface Iprops {

  params : {tourNo : string , companyName:"kuoni" | "Tumlare"}
}

export class TourInfo extends React.Component<Iprops,IState > {

    constructor(prop:Iprops){
        super(prop);
        this.state={ 
            tourDtl :{
                id: '',
            }
        };
       
    }

    public componentDidMount(){
        tourDetail.tourTourNumberGet(this.props.params.tourNo).then((tourDtl)=>{
               this.setState({tourDtl});
            });
     }

    public render() {
        return (
            <div className="container">
                <br />
                <p>
                  <Badge color="secondary">
                    <p>Test Tour<br />Test Agent<br /></p>
                    <p>18-09-2017-25-09-2017<br/>33+0(Pax+TC)</p>
              	 	</Badge>
                 </p>
                 <hr className="half-rule"/>
                 <Grid>
                   <Row>
                    <Col xs={6} md={4} >
                     <div className="tour_Details">						
                         <p> <Link to="/itinerary"><button>{this.state.tourDtl.id}</button></Link></p>
                         <p><b>Tour No:</b> {this.props.params.tourNo}</p>
                         <p><b>Company Name:</b> {this.state.tourDtl.backend}</p>
                     	</div>		
                    </Col>
                   </Row>
                  </Grid>
             </div>
           );
    }
}