import * as React from 'react';
import {  hashHistory,IndexRoute,Route,Router } from 'react-router';
import { App } from './App';
import { ItineraryData } from './itinerary';
import {TourInfo} from './TourInfo';
import TourList from './TourList';
export const AppRouter: React.StatelessComponent<{}> = () => {
  return (
    <Router history={hashHistory}>
      <Route path="/" component={App} >
        <IndexRoute component={TourList} />
        <Route path="/itinerary" component={ItineraryData} />
        <Route path="/TourInfo/:tourNo" component={TourInfo} />
      </Route>
    </Router>
  );
}
