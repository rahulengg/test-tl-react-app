import * as React from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';
import { Link } from 'react-router';
 import '../App.css';
// import '../css/mobile_app.css';
import back from '../images/back.png';
import home from '../images/home.png';
import logo from '../images/logo-mobile.png';
import logout from '../images/logout.png';


export const Header: React.StatelessComponent<{}> = () => {

  return (

      <div>
          
      <Navbar fixedTop={true} >  
       <Navbar.Header>
          <Navbar.Brand>
          <img src={logo} alt="gts" className="logo"/>    
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse   >
        <Nav className="pull-right">
      <NavItem eventKey={1} href="#">
      <img src={back} alt="back"/>


      </NavItem>
      <NavItem eventKey={2} href="#">
      <img src={home} alt="home"/>
      </NavItem>
      <NavItem eventKey={2} href="#">
      <img src={logout} alt="logout"/>
      </NavItem>
    </Nav>
        </Navbar.Collapse> 
 
      </Navbar>
    

  </div>

  );
}

	