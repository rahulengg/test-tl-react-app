import * as React from 'react';

import '../App.css';
class Footer extends React.Component {

public render() {
      return (
         <div>
             <div className="navbar navbar-default navbar-fixed-bottom">
               <div className="container-fluid">
                     <p> &copy; Copyright 2017 Kuoni Global Travel Services - All rights reserved. </p>
              </div>
         </div>

         </div>
      );
   }
}

export default Footer;
