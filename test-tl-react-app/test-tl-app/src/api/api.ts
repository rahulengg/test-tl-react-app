
// tslint:disable
/**
 * Kuoni Tumlare Tourleader API
 * Tourleader
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


import * as url from "url";
import * as portableFetch from "portable-fetch";
import { Configuration } from "./configuration";

const BASE_PATH = "http://localhost:8080/v1".replace(/\/+$/, "");

/**
 *
 * @export
 */
export const COLLECTION_FORMATS = {
    csv: ",",
    ssv: " ",
    tsv: "\t",
    pipes: "|",
};

/**
 *
 * @export
 * @interface FetchAPI
 */
export interface FetchAPI {
    (url: string, init?: any): Promise<Response>;
}

/**
 *
 * @export
 * @interface FetchArgs
 */
export interface FetchArgs {
    url: string;
    options: any;
}

/**
 *
 * @export
 * @class BaseAPI
 */
export class BaseAPI {
    protected configuration: Configuration;

    constructor(configuration?: Configuration, protected basePath: string = BASE_PATH, protected fetch: FetchAPI = portableFetch) {
        if (configuration) {
            this.configuration = configuration;
            this.basePath = configuration.basePath || this.basePath;
        }
    }
};

/**
 *
 * @export
 * @class RequiredError
 * @extends {Error}
 */
export class RequiredError extends Error {
    name: "RequiredError"
    constructor(public field: string, msg?: string) {
        super(msg);
    }
}

/**
 *
 * @export
 * @interface Itinerary
 */
export interface Itinerary {
    /**
     * Unique identifier representing a specific tour.
     * @type {string}
     * @memberof Itinerary
     */
    tourNumber?: string;
    /**
     * Display name of product.
     * @type {string}
     * @memberof Itinerary
     */
    agentName?: string;
    /**
     * Count of passengers.
     * @type {number}
     * @memberof Itinerary
     */
    paxCount?: number;
    /**
     * Count of additional passengers.
     * @type {number}
     * @memberof Itinerary
     */
    additionalPaxCount?: number;
    /**
     *
     * @type {Array<ItineraryItem>}
     * @memberof Itinerary
     */
    list?: Array<ItineraryItem>;
}

/**
 *
 * @export
 * @interface ItineraryItem
 */
export interface ItineraryItem {
    /**
     * Unique identifier representing a specific Itinerary Item
     * @type {string}
     * @memberof ItineraryItem
     */
    id?: string;
    /**
     * Start date time of itinerary detail - format: https://tools.ietf.org/html/rfc3339#section-5.6
     * @type {Date}
     * @memberof ItineraryItem
     */
    start?: Date;
    /**
     * End date time of itinerary detail - format: https://tools.ietf.org/html/rfc3339#section-5.6
     * @type {Date}
     * @memberof ItineraryItem
     */
    end?: Date;
    /**
     * Country in iso 3166-1-alpha-2 format - more information can be taken from API https://restcountries.eu
     * @type {string}
     * @memberof ItineraryItem
     */
    country?: string;
    /**
     * City name
     * @type {string}
     * @memberof ItineraryItem
     */
    city?: string;
}

/**
 *
 * @export
 * @interface ModelError
 */
export interface ModelError {
    /**
     *
     * @type {number}
     * @memberof ModelError
     */
    code?: number;
    /**
     *
     * @type {string}
     * @memberof ModelError
     */
    message?: string;
    /**
     *
     * @type {string}
     * @memberof ModelError
     */
    fields?: string;
}

/**
 *
 * @export
 * @interface Tour
 */
export interface Tour {
    /**
     * Unique identifier representing a specific Tour
     * @type {string}
     * @memberof Tour
     */
    id?: string;
    /**
     *
     * @type {string}
     * @memberof Tour
     */
    backend?: Tour.BackendEnum;
}

/**
 * @export
 * @namespace Tour
 */
export namespace Tour {
    /**
     * @export
     * @enum {string}
     */
    export enum BackendEnum {
        Kuoni = <any> 'kuoni',
        Tumlare = <any> 'tumlare'
    }
}


/**
 * TourleaderApi - fetch parameter creator
 * @export
 */
export const TourleaderApiFetchParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
         * @summary Tour Information
         * @param {'kuoni' | 'tumlare'} backend Backend System Name
         * @param {string} tourNumber Tour number
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        itineraryBackendTourNumberGet(backend: 'kuoni' | 'tumlare', tourNumber: string, options: any = {}): FetchArgs {
            // verify required parameter 'backend' is not null or undefined
            if (backend === null || backend === undefined) {
                throw new RequiredError('backend','Required parameter backend was null or undefined when calling itineraryBackendTourNumberGet.');
            }
            // verify required parameter 'tourNumber' is not null or undefined
            if (tourNumber === null || tourNumber === undefined) {
                throw new RequiredError('tourNumber','Required parameter tourNumber was null or undefined when calling itineraryBackendTourNumberGet.');
            }
            const localVarPath = `/itinerary/{backend}/{tour_number}`
                .replace(`{${"backend"}}`, encodeURIComponent(String(backend)))
                .replace(`{${"tour_number"}}`, encodeURIComponent(String(tourNumber)));
            const localVarUrlObj = url.parse(localVarPath, true);
            const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            localVarUrlObj.query = Object.assign({}, localVarUrlObj.query, localVarQueryParameter, options.query);
            // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
            delete localVarUrlObj.search;
            localVarRequestOptions.headers = Object.assign({Accept: 'application/json'}, localVarHeaderParameter, options.headers);

            return {
                url: url.format(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
         * @summary Tour Information
         * @param {string} tourNumber Tour number
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        tourTourNumberGet(tourNumber: string, options: any = {}): FetchArgs {
            // verify required parameter 'tourNumber' is not null or undefined
            if (tourNumber === null || tourNumber === undefined) {
                throw new RequiredError('tourNumber','Required parameter tourNumber was null or undefined when calling tourTourNumberGet.');
            }
            const localVarPath = `/tour/{tour_number}`
                .replace(`{${"tour_number"}}`, encodeURIComponent(String(tourNumber)));
            const localVarUrlObj = url.parse(localVarPath, true);
            const localVarRequestOptions = Object.assign({ method: 'GET' }, options);
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            localVarUrlObj.query = Object.assign({}, localVarUrlObj.query, localVarQueryParameter, options.query);
            // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
            delete localVarUrlObj.search;
            localVarRequestOptions.headers = Object.assign({Accept: 'application/json'}, localVarHeaderParameter, options.headers);

            return {
                url: url.format(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * The Tours endpoint returns list of tourleader's tour numbers
         * @summary Itinerary List
         * @param {string} tourleaderId Tourleader identification
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        tourleaderTourleaderIdToursGet(tourleaderId: string, options: any = {}): FetchArgs {
            // verify required parameter 'tourleaderId' is not null or undefined
            if (tourleaderId === null || tourleaderId === undefined) {
                throw new RequiredError('tourleaderId','Required parameter tourleaderId was null or undefined when calling tourleaderTourleaderIdToursGet.');
            }
            const localVarPath = `/tourleader/{tourleader_id}/tours`
                .replace(`{${"tourleader_id"}}`, encodeURIComponent(String(tourleaderId)));
            const localVarUrlObj = url.parse(localVarPath, true);
            const localVarRequestOptions = Object.assign({ method: 'GET'}, options);
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            localVarUrlObj.query = Object.assign({}, localVarUrlObj.query, localVarQueryParameter, options.query);
            // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
            delete localVarUrlObj.search;
            localVarRequestOptions.headers = Object.assign({Accept: 'application/json'}, localVarHeaderParameter, options.headers);

            return {
                url: url.format(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * TourleaderApi - functional programming interface
 * @export
 */
export const TourleaderApiFp = function(configuration?: Configuration) {
    return {
        /**
         * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
         * @summary Tour Information
         * @param {'kuoni' | 'tumlare'} backend Backend System Name
         * @param {string} tourNumber Tour number
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        itineraryBackendTourNumberGet(backend: 'kuoni' | 'tumlare', tourNumber: string, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Itinerary> {
            const localVarFetchArgs = TourleaderApiFetchParamCreator(configuration).itineraryBackendTourNumberGet(backend, tourNumber, options);
            return (fetch: FetchAPI = portableFetch, basePath: string = BASE_PATH) => {
                return fetch(basePath + localVarFetchArgs.url, localVarFetchArgs.options).then((response) => {
                    if (response.status == 200 ) {
                        console.log('success:-'+response);
                        return response.json();
                    } else {
                        throw response;
                    }
                });
            };
        },
        /**
         * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
         * @summary Tour Information
         * @param {string} tourNumber Tour number
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        tourTourNumberGet(tourNumber: string, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Tour> {
            const localVarFetchArgs = TourleaderApiFetchParamCreator(configuration).tourTourNumberGet(tourNumber, options);
            return (fetch: FetchAPI = portableFetch, basePath: string = BASE_PATH) => {
                return fetch(basePath + localVarFetchArgs.url, localVarFetchArgs.options).then((response) => {
                    if (response.status == 200) {
                        return response.json();
                    } else {
                        throw response;
                    }
                });
            };
        },
        /**
         * The Tours endpoint returns list of tourleader's tour numbers
         * @summary Itinerary List
         * @param {string} tourleaderId Tourleader identification
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        tourleaderTourleaderIdToursGet(tourleaderId: string, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Array<string>> {

            const localVarFetchArgs = TourleaderApiFetchParamCreator(configuration).tourleaderTourleaderIdToursGet(tourleaderId, options);

            return (fetch: FetchAPI = portableFetch, basePath: string = BASE_PATH) => {
                return fetch(basePath + localVarFetchArgs.url, localVarFetchArgs.options).then((response) => {
                      console.log('success:-'+response);
                    if ( response.status == 200) {
                       console.log('success:-'+response);
                        return response.json();
                    } else {
                       console.log('error:-'+response);
                        throw response;
                    }
                });
            };
        },
    }
};

/**
 * TourleaderApi - factory interface
 * @export
 */
export const TourleaderApiFactory = function (configuration?: Configuration, fetch?: FetchAPI, basePath?: string) {
    return {
        /**
         * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
         * @summary Tour Information
         * @param {'kuoni' | 'tumlare'} backend Backend System Name
         * @param {string} tourNumber Tour number
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        itineraryBackendTourNumberGet(backend: 'kuoni' | 'tumlare', tourNumber: string, options?: any) {
            return TourleaderApiFp(configuration).itineraryBackendTourNumberGet(backend, tourNumber, options)(fetch, basePath);
        },
        /**
         * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
         * @summary Tour Information
         * @param {string} tourNumber Tour number
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        tourTourNumberGet(tourNumber: string, options?: any) {
            return TourleaderApiFp(configuration).tourTourNumberGet(tourNumber, options)(fetch, basePath);
        },
        /**
         * The Tours endpoint returns list of tourleader's tour numbers
         * @summary Itinerary List
         * @param {string} tourleaderId Tourleader identification
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        tourleaderTourleaderIdToursGet(tourleaderId: string, options?: any) {
            return TourleaderApiFp(configuration).tourleaderTourleaderIdToursGet(tourleaderId, options)(fetch, basePath);
        },
    };
};

/**
 * TourleaderApi - object-oriented interface
 * @export
 * @class TourleaderApi
 * @extends {BaseAPI}
 */
export class TourleaderApi extends BaseAPI {
    /**
     * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
     * @summary Tour Information
     * @param {'kuoni' | 'tumlare'} backend Backend System Name
     * @param {string} tourNumber Tour number
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof TourleaderApi
     */
    public itineraryBackendTourNumberGet(backend: 'kuoni' | 'tumlare', tourNumber: string, options?: any) {
        return TourleaderApiFp(this.configuration).itineraryBackendTourNumberGet(backend, tourNumber, options)(this.fetch, this.basePath);
    }

    /**
     * The Itinerary endpoint returns detail information about tour e.g. final backend for tour number
     * @summary Tour Information
     * @param {string} tourNumber Tour number
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof TourleaderApi
     */
    public tourTourNumberGet(tourNumber: string, options?: any) {
        return TourleaderApiFp(this.configuration).tourTourNumberGet(tourNumber, options)(this.fetch, this.basePath);
    }

    /**
     * The Tours endpoint returns list of tourleader's tour numbers
     * @summary Itinerary List
     * @param {string} tourleaderId Tourleader identification
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof TourleaderApi
     */
    public tourleaderTourleaderIdToursGet(tourleaderId: string, options?: any) {
        console.log('Hi');
        return TourleaderApiFp(this.configuration).tourleaderTourleaderIdToursGet(tourleaderId, options)(this.fetch, this.basePath);
    }

}
